require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs << 'debian/test'
  t.test_files = FileList['debian/test/test_pure_ruby.rb']
  t.verbose = true
end

Gem2Deb::Rake::TestTask.new do |t|
  t.libs << 'debian/test'
  t.test_files = FileList['debian/test/test_jaro_winkler.rb']
  t.verbose = true
end
